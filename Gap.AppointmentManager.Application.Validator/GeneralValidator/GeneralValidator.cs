﻿namespace Gap.AppointmentManager.Application.Validator.GeneralValidator
{
    using FluentValidation;
    using FluentValidation.Results;
    using Gap.AppointmentManager.Application.Validator.Validators;
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Helper;
    using Gap.AppointmentManager.Domain.Interfaces.Application;

    public class GeneralValidator : IGeneralValidator
    {
        public PatientValidator PatientValidator { get; set; }

        public AppointmentValidator AppointmentValidator { get; set; }

        public GeneralValidator(PatientValidator PatientValidator)
        {
            this.PatientValidator = PatientValidator;
        }

        public void ExecutePatientValidator(Patient patient)
        {
            ValidationResult results = PatientValidator.Validate(patient);

            if (!results.IsValid)
            {
                string allMessages = results.ToString("\n");
                throw new GapException("ExecutePatientValidator", allMessages);
            }
        }

        public void ExcuteAddingAppointmentValidator(Appointment appointment)
        {
            ValidationResult results = AppointmentValidator.Validate(appointment, ruleSet: "Adding");
            if (!results.IsValid)
            {
                string allMessages = results.ToString("\n");
                throw new GapException("ExcuteAddingAppointmentValidator", allMessages);
            }
        }

        public void ExcuteCancelingAppointmentValidator(Appointment appointment)
        {
            ValidationResult results = AppointmentValidator.Validate(appointment, ruleSet: "Canceling");
            if (!results.IsValid)
            {
                string allMessages = results.ToString("\n");
                throw new GapException("ExcuteCancelingAppointmentValidator", allMessages);
            }
        }
    }
}

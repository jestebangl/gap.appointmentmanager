﻿namespace Gap.AppointmentManager.Application.Validator.Validators
{
    using FluentValidation;
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Enum;
    using Gap.AppointmentManager.Infrastructure.DataAccess.Repository;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public class AppointmentValidator : AbstractValidator<Appointment>
    {
        public UnitOfWork UnitOfWork { get; set; }

        public AppointmentValidator(UnitOfWork UnitOfWork)
        {
            this.UnitOfWork = UnitOfWork;

            RuleSet("Adding", () =>
            {
                RuleFor(appointmentModel => appointmentModel).Must(ExistPatientWithAppoitnmentSameDay).WithMessage(ErrorMessagesEnum.CantAddNewAppointment.ToDescriptionString());
            });

            RuleSet("Canceling", () =>
            {
                RuleFor(appointmentModel => appointmentModel).Must(IsAvailableCancelAppointment).WithMessage(ErrorMessagesEnum.CantCancelAppointment.ToDescriptionString());
            });
        }

        private bool ExistPatientWithAppoitnmentSameDay(Appointment appointment)
        {
            if (appointment != null)
            {
                Expression<Func<Appointment, bool>> filter = x => x.AppointmentDate == appointment.AppointmentDate && x.PatientId == appointment.PatientId;
                var exists = UnitOfWork.AppointmentRepository.Get(filter).Any();

                return exists ? false : true;
            }
            return false;
        }

        private bool IsAvailableCancelAppointment(Appointment appointment)
        {
            if (appointment.AppointmentDate == DateTime.Now.Date)
            {
                return false;
            }

            return true;
        }

    }
}

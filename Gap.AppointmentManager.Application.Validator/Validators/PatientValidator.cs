﻿namespace Gap.AppointmentManager.Application.Validator.Validators
{
    using FluentValidation;
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Enum;
    using Gap.AppointmentManager.Infrastructure.DataAccess.Repository;
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public class PatientValidator : AbstractValidator<Patient>
    {
        public UnitOfWork UnitOfWork { get; set; }

        public PatientValidator(UnitOfWork UnitOfWork)
        {
            this.UnitOfWork = UnitOfWork;
            this.CascadeMode = CascadeMode.Continue;

            this.RuleFor(patientModel => patientModel.DocumentNumber).Must(ExistPatientWithSameDocumentNumber).WithMessage(ErrorMessagesEnum.PatientWithSameDocumentNumber.ToDescriptionString());
        }

        private bool ExistPatientWithSameDocumentNumber(string documentNumber)
        {
            if (!string.IsNullOrEmpty(documentNumber))
            {
                Expression<Func<Patient, bool>> filter = x => x.DocumentNumber.Equals(documentNumber);
                var exists = UnitOfWork.PatientRepository.Get(filter).Any();

                return exists ? false : true;
            }
            return false;
        }
    }
}

﻿namespace Gap.AppointmentManager.Domain.Interfaces.Application
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities;
    using System.Collections.Generic;

    public interface IPatientHelper
    {
        PatientModel GetPatientModel(Patient patient);

        List<PatientModel> GetPatientModelList(IEnumerable<Patient> patientList);

        Patient GetPatientModelToUpdate(Patient newReference, Patient oldReference);
    }
}

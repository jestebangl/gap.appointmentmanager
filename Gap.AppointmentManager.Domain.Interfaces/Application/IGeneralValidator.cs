﻿namespace Gap.AppointmentManager.Domain.Interfaces.Application
{
    using Gap.AppointmentManager.Domain.AppointmentEF;

    public interface IGeneralValidator
    {
        void ExecutePatientValidator(Patient patient);

        void ExcuteAddingAppointmentValidator(Appointment appointment);

        void ExcuteCancelingAppointmentValidator(Appointment appointment);
    }
}

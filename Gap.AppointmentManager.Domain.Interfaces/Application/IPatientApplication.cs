﻿namespace Gap.AppointmentManager.Domain.Interfaces.Application
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities;
    using System.Collections.Generic;

    public interface IPatientApplication
    {
        bool AddPatient(Patient patient);

        bool UpdatePatient(Patient patient);

        List<PatientModel> GetPatientList();

        PatientModel GetPatientById(long patientId);
    }
}

﻿namespace Gap.AppointmentManager.Domain.Interfaces.Application
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using System.Collections.Generic;

    public interface IGeneralUtilitiesApplication
    {
        List<DocumentTypeModel> GetDocumentType();

        List<AppointmentTypeModel> GetAppointmentType();
    }
}

﻿namespace Gap.AppointmentManager.Domain.Interfaces.Application
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using System.Collections.Generic;

    public interface IAppointmentApplication
    {
        bool AddAppointment(Appointment appointment);

        List<AppointmentModel> GetAppointmentList();

        bool DeleteAppointment(long appointmenId);

        Appointment GetAppointmentById(long appointmenId);

    }
}

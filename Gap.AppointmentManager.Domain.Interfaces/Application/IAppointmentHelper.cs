﻿namespace Gap.AppointmentManager.Domain.Interfaces.Application
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using System.Collections.Generic;


    public interface IAppointmentHelper
    {
        List<AppointmentModel> GetAppointmentModelList(IEnumerable<Appointment> appointments);
    }
}

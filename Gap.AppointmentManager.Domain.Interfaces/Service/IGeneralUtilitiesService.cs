﻿namespace Gap.AppointmentManager.Domain.Interfaces.Service
{
    using Gap.AppointmentManager.Domain.Entities.Models;
    using System.Collections.Generic;

    public interface IGeneralUtilitiesService
    {
        List<DocumentTypeModel> GetDocumentTypes();

        List<AppointmentTypeModel> GetAppointmentType();
    }
}

﻿namespace Gap.AppointmentManager.Domain.Interfaces.Service
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities;
    using System.Collections.Generic;

    public interface IPatientService
    {
        bool AddPatient(Patient patient);

        bool UpdatePatient(Patient patient);

        List<PatientModel> GetPatientList();

        PatientModel GetPatientById(long patientId);
    }
}

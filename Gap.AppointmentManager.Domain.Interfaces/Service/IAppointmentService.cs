﻿namespace Gap.AppointmentManager.Domain.Interfaces.Service
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using System.Collections.Generic;


    public interface IAppointmentService
    {
        bool AddAppointment(Appointment appointment);

        List<AppointmentModel> GetAppointmentList();

        bool RemoveAppointment(long appointmentId);
    }
}

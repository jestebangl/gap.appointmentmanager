﻿namespace Gap.AppointmentManager.Service.Orchestrator.Appointment
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Enum;
    using Gap.AppointmentManager.Domain.Entities.Helper;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using Gap.AppointmentManager.Domain.Interfaces.Application;
    using Gap.AppointmentManager.Domain.Interfaces.Service;
    using System;
    using System.Collections.Generic;

    public class AppointmentService : IAppointmentService
    {
        public IAppointmentApplication AppointmentApplication { get; set; }

        public IGeneralValidator GeneralValidator { get; set; }

        public AppointmentService(IGeneralValidator GeneralValidator, IAppointmentApplication AppointmentApplication)
        {
            this.AppointmentApplication = AppointmentApplication;
            this.GeneralValidator = GeneralValidator;
        }

        public bool AddAppointment(Appointment appointment)
        {
            try
            {
                GeneralValidator.ExcuteAddingAppointmentValidator(appointment);
                return AppointmentApplication.AddAppointment(appointment);
            }
            catch (GapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GapException("AddAppointment", ErrorMessagesEnum.UnexpectedError, ex);
            }
        }

        public List<AppointmentModel> GetAppointmentList()
        {
            try
            {
                return AppointmentApplication.GetAppointmentList();
            }
            catch (GapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GapException("GetPatientList", ErrorMessagesEnum.UnexpectedError, ex);

            }
        }

        public bool RemoveAppointment(long appointmentId)
        {
            try
            {
                var appointment = AppointmentApplication.GetAppointmentById(appointmentId);
                GeneralValidator.ExcuteCancelingAppointmentValidator(appointment);
                return AppointmentApplication.DeleteAppointment(appointmentId);
            }
            catch (GapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GapException("GetPatientList", ErrorMessagesEnum.UnexpectedError, ex);

            }
        }
    }
}

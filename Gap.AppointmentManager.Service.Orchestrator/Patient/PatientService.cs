﻿namespace Gap.AppointmentManager.Service.Orchestrator.Patient
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities;
    using Gap.AppointmentManager.Domain.Entities.Enum;
    using Gap.AppointmentManager.Domain.Entities.Helper;
    using Gap.AppointmentManager.Domain.Interfaces.Application;
    using Gap.AppointmentManager.Domain.Interfaces.Service;
    using System;
    using System.Collections.Generic;

    public class PatientService : IPatientService
    {
        public IPatientApplication PatientApplication { get; set; }
        public IGeneralValidator GeneralValidator { get; set; }

        public PatientService(IPatientApplication PatientApplication, IGeneralValidator GeneralValidator)
        {
            this.PatientApplication = PatientApplication;
            this.GeneralValidator = GeneralValidator;
        }

        public bool AddPatient(Patient patient)
        {
            try
            {
                GeneralValidator.ExecutePatientValidator(patient);
                return PatientApplication.AddPatient(patient);
            }
            catch (GapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GapException("AddPatient", ErrorMessagesEnum.AddingPatientError, ex);
            }
        }

        public PatientModel GetPatientById(long patientId)
        {
            try
            {
                return PatientApplication.GetPatientById(patientId);
            }
            catch (GapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GapException("GetPatientById", ErrorMessagesEnum.GettingPatientsByIdError, ex);
            }
        }

        public List<PatientModel> GetPatientList()
        {
            try
            {
                return PatientApplication.GetPatientList();
            }
            catch (GapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GapException("GetPatientList", ErrorMessagesEnum.GettingPatientsListError, ex);
            }
        }

        public bool UpdatePatient(Patient patient)
        {
            try
            {
                return PatientApplication.UpdatePatient(patient);
            }
            catch (GapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GapException("UpdatePatient", ErrorMessagesEnum.UpdatingPatientError, ex);
            }
        }
    }
}

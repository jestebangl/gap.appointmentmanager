﻿namespace Gap.AppointmentManager.Service.Orchestrator.GeneralUtilities
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Enum;
    using Gap.AppointmentManager.Domain.Entities.Helper;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using Gap.AppointmentManager.Domain.Interfaces.Application;
    using Gap.AppointmentManager.Domain.Interfaces.Service;
    using System;
    using System.Collections.Generic;

    public class GeneralUtilitiesService : IGeneralUtilitiesService
    {
        public IGeneralUtilitiesApplication GeneralUtilitiesApplication { get; set; }

        public GeneralUtilitiesService(IGeneralUtilitiesApplication GeneralUtilitiesApplication)
        {
            this.GeneralUtilitiesApplication = GeneralUtilitiesApplication;
        }

        public List<DocumentTypeModel> GetDocumentTypes()
        {
            try
            {
                return GeneralUtilitiesApplication.GetDocumentType();
            }
            catch (GapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GapException("GetDocumentTypes", ErrorMessagesEnum.GetDocumentTypeError, ex);
            }
        }

        public List<AppointmentTypeModel> GetAppointmentType()
        {
            try
            {
                return GeneralUtilitiesApplication.GetAppointmentType();
            }
            catch (GapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new GapException("GetAppointmentType", ErrorMessagesEnum.GetAppointmentTypeError, ex);
            }
        }
    }
}

﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Gap.AppointmentManager.Service.DependencyResolution.Installers
{
    public class InfraestructureInstaller : IWindsorInstaller
    {
        public InfraestructureInstaller() { }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

        }
    }
}

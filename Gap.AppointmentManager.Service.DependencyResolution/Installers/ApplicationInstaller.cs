﻿namespace Gap.AppointmentManager.Service.DependencyResolution.Installers
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Gap.Appointment.Application.Operator.Appointment;
    using Gap.Appointment.Application.Operator.GeneralUtilities;
    using Gap.Appointment.Application.Operator.Helper;
    using Gap.Appointment.Application.Operator.Patient;
    using Gap.AppointmentManager.Domain.Interfaces.Application;
    using Gap.AppointmentManager.Infrastructure.DataAccess.Repository;

    public class ApplicationInstaller : IWindsorInstaller
    {
        public UnitOfWork UnitOfWork { get; set; }

        public ApplicationInstaller(UnitOfWork UnitOfWork)
        {
            this.UnitOfWork = UnitOfWork;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IPatientHelper>().ImplementedBy<PatientHelper>().Named("PatientHelper")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IAppointmentHelper>().ImplementedBy<AppointmentHelper>().Named("AppointmentHelper")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IGeneralUtilitiesApplication>().ImplementedBy<GeneralUtilitiesApplication>().Named("GeneralUtilitiesApplication")
                    .DependsOn(Dependency.OnValue("UnitOfWork", this.UnitOfWork))
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IPatientApplication>().ImplementedBy<PatientApplication>().Named("PatientApplication")
                    .DependsOn(Dependency.OnValue("UnitOfWork", this.UnitOfWork))
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IAppointmentApplication>().ImplementedBy<AppointmentApplication>().Named("AppointmentApplication")
                    .DependsOn(Dependency.OnValue("UnitOfWork", this.UnitOfWork))
                    .LifeStyle.HybridPerWebRequestPerThread()

                );
        }
    }
}

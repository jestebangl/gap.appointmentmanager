﻿namespace Gap.AppointmentManager.Service.DependencyResolution.Installers
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Gap.AppointmentManager.Application.Validator.GeneralValidator;
    using Gap.AppointmentManager.Application.Validator.Validators;
    using Gap.AppointmentManager.Domain.Interfaces.Application;


    public class ApplicationValidatorInstaller : IWindsorInstaller
    {
        public PatientValidator PatientValidator { get; set; }

        public AppointmentValidator AppointmentValidator { get; set; }

        public ApplicationValidatorInstaller(PatientValidator PatientValidator, AppointmentValidator AppointmentValidator)
        {
            this.PatientValidator = PatientValidator;
            this.AppointmentValidator = AppointmentValidator;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IGeneralValidator>().ImplementedBy<GeneralValidator>().Named("GeneralValidator")
                    .DependsOn(Dependency.OnValue("PatientValidator", this.PatientValidator))
                    .DependsOn(Dependency.OnValue("AppointmentValidator", this.AppointmentValidator))
                    .LifeStyle.HybridPerWebRequestPerThread()

                    );
        }
    }
}

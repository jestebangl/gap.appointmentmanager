﻿namespace Gap.AppointmentManager.Service.DependencyResolution.Installers
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using Gap.AppointmentManager.Domain.Interfaces.Service;
    using Gap.AppointmentManager.Service.Orchestrator.Appointment;
    using Gap.AppointmentManager.Service.Orchestrator.GeneralUtilities;
    using Gap.AppointmentManager.Service.Orchestrator.Patient;

    public class ServiceInstaller : IWindsorInstaller
    {
        public ServiceInstaller()
        {
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Component.For<IGeneralUtilitiesService>().ImplementedBy<GeneralUtilitiesService>().Named("GeneralUtilitiesService")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IPatientService>().ImplementedBy<PatientService>().Named("PatientService")
                    .LifeStyle.HybridPerWebRequestPerThread(),

                Component.For<IAppointmentService>().ImplementedBy<AppointmentService>().Named("AppointmentService")
                    .LifeStyle.HybridPerWebRequestPerThread()
                );
        }
    }
}

namespace Gap.AppointmentManager.Domain.AppointmentEF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AppointmentDBModel : DbContext
    {
        public AppointmentDBModel()
            : base("name=AppointmentDBModel")
        {
        }

        public virtual DbSet<Appointment> Appointments { get; set; }
        public virtual DbSet<AppointmentType> AppointmentTypes { get; set; }
        public virtual DbSet<DocumentType> DocumentTypes { get; set; }
        public virtual DbSet<Patient> Patients { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AppointmentType>()
                .Property(e => e.AppointmentTypeName)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .Property(e => e.DocumentName)
                .IsUnicode(false);

            modelBuilder.Entity<DocumentType>()
                .HasMany(e => e.Patients)
                .WithOptional(e => e.DocumentType)
                .HasForeignKey(e => e.DocuementTypeId);

            modelBuilder.Entity<Patient>()
                .Property(e => e.PatientName)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.PatientLastName)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.DocumentNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Patient>()
                .Property(e => e.CellPhoneNumber)
                .IsUnicode(false);
        }
    }
}

namespace Gap.AppointmentManager.Domain.AppointmentEF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Appointment")]
    public partial class Appointment
    {
        public long AppointmentId { get; set; }

        public long? AppointmentTypeId { get; set; }

        public long? PatientId { get; set; }

        public DateTime? AppointmentDate { get; set; }

        public virtual AppointmentType AppointmentType { get; set; }

        public virtual Patient Patient { get; set; }
    }
}

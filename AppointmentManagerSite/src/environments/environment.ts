declare var data: any;

export const environment = {
  production: false,
  GetPatientList: data.basePathWebApi + 'api/patient/getpatientlist',
  GetDocumentType: data.basePathWebApi + 'api/generalutilitites/getdocumenttype',
  AddPatient: data.basePathWebApi + 'api/patient/addpatient',
  GetPatientById: data.basePathWebApi + 'api/patient/getpatientbyid/?patientId=',
  UpdatePatient: data.basePathWebApi + 'api/patient/updatepatient',
  GetAppointmentList: data.basePathWebApi + 'api/appointment/getappointmentlist',
  GetAppointmentType: data.basePathWebApi + 'api/generalutilitites/getappointmenttype',
  AddAppointment: data.basePathWebApi + 'api/appointment/addappointment',
  RemoveAppointment: data.basePathWebApi + 'api/appointment/removeappointment/?appointmentId='


};

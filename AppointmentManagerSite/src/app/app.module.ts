import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { ConectorApiHelper } from './Core/Helper/conectorApiHelper';
import { appRoutingProviders, routing } from './app-routing';
import { HttpModule } from '@angular/http';
import { ToasterModule } from 'angular2-toaster';
import { NgxLoadingModule } from 'ngx-loading';
import { PatientComponent } from './patient/patient.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { ModalModule } from 'ngx-bootstrap';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    PatientComponent,
    AppointmentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    NgxLoadingModule.forRoot({}),
    ToasterModule.forRoot(),
    ModalModule.forRoot()
  ],
  providers: [appRoutingProviders, ConectorApiHelper],
  bootstrap: [AppComponent]
})
export class AppModule { }

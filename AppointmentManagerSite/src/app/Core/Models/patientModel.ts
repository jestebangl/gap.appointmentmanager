export class PatientModel {
    public PatientId: number;
    public PatientName: string;
    public PatientLastName: string;
    public DocuementTypeId: number;
    public DocumentNumber: string;
    public Email: string;
    public CellPhoneNumber: string;

    constructor() { }

}

export const MessagesHelper = {
    PacientsNotFound: 'Actualmente no hay paciente ingresados',
    AppointmentsNotFound: 'Actualmente no hay citas ingresadas',
    ValueRequired: 'El valor es requerido',
    EmailAlert: 'Valide que el correo electronico ingresado este correcto',
};

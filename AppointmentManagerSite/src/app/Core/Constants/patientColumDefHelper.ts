export const PatientColumDefHelper = [
    {headerName: 'PatientName', field: 'Nombre', sortable: true, filter: true, resizable: true},
    {headerName: 'PatientLastName', field: 'Apellido', sortable: true, filter: true, resizable: true},
    {headerName: 'DocumentNumber', field: 'Numero de Documento', sortable: true, filter: true, resizable: true},
    {headerName: 'Email', field: 'Correo', sortable: true, filter: true, resizable: true},
    {headerName: 'CellPhoneNumber', field: 'Numero Celular', sortable: true, filter: true, resizable: true}
];

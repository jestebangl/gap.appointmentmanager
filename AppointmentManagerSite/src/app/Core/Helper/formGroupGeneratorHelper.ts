import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AppointmentModel } from '../Models/appointmentModel';
import { PatientModel } from '../Models/PatientModel';

export class FormGroupGenerator {

    public InicialicePatientForm(patient: PatientModel) {
        return new FormGroup({
            PatientName: new FormControl(
                patient.PatientName,
                [
                    Validators.required
                ]
            ),
            PatientLastName: new FormControl(
                patient.PatientLastName,
                [
                    Validators.required
                ]
            ),
            DocumentNumber: new FormControl(
                patient.DocumentNumber,
                [
                    Validators.required
                ]
            ),
            DocuementTypeId: new FormControl(
                patient.DocuementTypeId,
                [
                    Validators.required
                ]
            ),
            Email: new FormControl(
                patient.Email,
                [
                    Validators.required
                ]
            ),
            CellPhoneNumber: new FormControl(
                patient.CellPhoneNumber,
                [
                    Validators.required
                ]
            )
        });
    }

    public InicialiceAppointmentForm(appointment: AppointmentModel) {
        return new FormGroup({
            AppointmentDate: new FormControl(
                appointment.AppointmentDate,
                [
                    Validators.required
                ]
            ),
            AppointmentTypeId: new FormControl(
                appointment.AppointmentTypeId,
                [
                    Validators.required
                ]
            ),
            PatientId: new FormControl(
                appointment.PatientId,
                [
                    Validators.required
                ]
            ),
        });
    }
}

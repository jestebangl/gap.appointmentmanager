import { FormGroup } from '@angular/forms';

export class GeneralUtilitiesHelper {

    constructor() { }

    public ValidateErrorControl(controlName: string, form: FormGroup) {
        return (
          form !== undefined && form.controls[controlName].errors
        );
    }

    public ShowMessageError(controlName: string, form: FormGroup) {
        return {
          'invalid-feedback': form !== undefined && form.controls[controlName].errors
        };
    }

    public ValidateCSSControl(controlName: string, form: FormGroup) {
        return {
          'is-valid': form !== undefined && !form.controls[controlName].errors,
          'is-invalid': form !== undefined && form.controls[controlName].errors,
          'form-control': true
        };
      }

}


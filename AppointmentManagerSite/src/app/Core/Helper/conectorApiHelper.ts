import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { EncriptToolHelper } from './encriptToolHelper';



@Injectable()
export class ConectorApiHelper {

  encriptTool: EncriptToolHelper;

  constructor(private http: Http) {
    this.encriptTool = new EncriptToolHelper();
  }


  public ResolverPeticionGet(url) {
    return new Promise((resolve, reject) => {
      this.http.get(url, { headers: this.headersREST(true) })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  public ResolverPeticionPost(url, params) {
    const strParams = JSON.stringify(params);
    const request = this.encriptTool.$Encrypted(strParams).toString();
    return new Promise((resolve, reject) => {
      this.http.post(url, request, { headers: this.headersREST(true) })
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  public ObtenerBodyRespuesta(data) {
    const answerStringify = JSON.stringify(data);
    const answerStringifyParce = JSON.parse(answerStringify);

    if (answerStringifyParce._body !== undefined) {
      const body = JSON.parse(answerStringifyParce._body);
      if (body.EsExitoso === true) {
        return JSON.parse(this.encriptTool.$Decrypted(body.Resultado.toString()));
      } else {
        return body.Errors;
      }
    }
  }

  private headersREST(esCifrado): Headers {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('Access-Control-Allow-Origin', '*');
    myHeaders.append('EsCifrado', esCifrado);
    return myHeaders;
  }

  public ObtenerBodyRespuestaError(data) {
    const answerStringify = JSON.stringify(data);
    const answerStringifyParce = JSON.parse(answerStringify);
    if (answerStringifyParce._body !== undefined) {
      const body = JSON.parse(answerStringifyParce._body);
      if (body.EsExitoso === false) {
        const errorBody = body.Errors[0].toString();
        return errorBody;
      } else {
        return body.Errors;
      }
    }
  }

}

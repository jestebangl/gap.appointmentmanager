import { FormGroupGenerator } from '../Helper/formGroupGeneratorHelper';
import { GeneralUtilitiesHelper } from '../Helper/generalUtilitiesHelper';
import { PatientModel } from '../Models/PatientModel';
import { AppointmentModel } from '../Models/appointmentModel';

export function getPatientFormGroup(patient) {
    return new FormGroupGenerator().InicialicePatientForm(patient);
}

export function getAppointmentFormGroup(appointment: AppointmentModel) {
    return new FormGroupGenerator().InicialiceAppointmentForm(appointment);
}

export function getGeneralUtilities() {
    return new GeneralUtilitiesHelper();
}

export function getPatientModel() {
    return new PatientModel();
}

export function getAppointmentModel() {
    return new AppointmentModel();
}

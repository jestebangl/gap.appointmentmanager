import { Component, OnInit } from '@angular/core';
import { AppointmentColumDefHelper } from '../Core/Constants/appointmentColumnDefHelper';
import { ToasterService } from 'angular2-toaster';
import { ConectorApiHelper } from '../Core/Helper/conectorApiHelper';
import { environment } from 'src/environments/environment';
import { FormGroup } from '@angular/forms';
import { AppointmentModel } from '../Core/Models/appointmentModel';
import { getAppointmentModel, getAppointmentFormGroup, getGeneralUtilities } from '../Core/Factory/ProjectFactory';
import { GeneralUtilitiesHelper } from '../Core/Helper/generalUtilitiesHelper';
import { MessagesHelper } from '../Core/Constants/messagesHelper';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css']
})
export class AppointmentComponent implements OnInit {

  public loading = false;
  public appointmentList: any;
  public APPOINTMENT_COLUMN_DEF = AppointmentColumDefHelper;
  public MESSAGGES_HELPER = MessagesHelper;
  public appointmentForm: FormGroup;
  public currentAppointment: AppointmentModel;
  public generalUtilities: GeneralUtilitiesHelper;
  public appointmentTypeList: any;
  public patientList: any;

  private toasterService: ToasterService;
  
  constructor(private conectorApiHelper: ConectorApiHelper,
    toasterService: ToasterService) { 
      this.toasterService = toasterService;
      this.currentAppointment = getAppointmentModel();
      this.appointmentForm = getAppointmentFormGroup(this.currentAppointment);
      this.generalUtilities = getGeneralUtilities();

      Promise.all([this.GetAppointmentList(), this.GetAppointmentTypeList(), this.GetPacientsList()]);
      
  }

  ngOnInit() {
  }

  public async GetAppointmentList() {
    this.loading = true;
    try {
      const respuesta = await this.conectorApiHelper.ResolverPeticionGet(environment.GetAppointmentList)
      this.appointmentList = this.conectorApiHelper.ObtenerBodyRespuesta(respuesta);
      console.log('lista', this.appointmentList)
    } catch (error) {
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    } finally {
      this.loading = false; 
    }
  }

  public async GetAppointmentTypeList() {
    this.loading = true;
    try {
      const respuesta = await this.conectorApiHelper.ResolverPeticionGet(environment.GetAppointmentType);
      this.appointmentTypeList = this.conectorApiHelper.ObtenerBodyRespuesta(respuesta);
    } catch (error) {
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    } finally {
      this.loading = false;
    }
  }

  private async GetPacientsList() {
    this.loading = true;
    try {
      const respuesta = await this.conectorApiHelper.ResolverPeticionGet(environment.GetPatientList);
      this.patientList = this.conectorApiHelper.ObtenerBodyRespuesta(respuesta);
    } catch (error) {
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    } finally {
      this.loading = false;
    }
  }

  public async AddNewAppointment() {
    this.loading = true;
    try {
      const response = this.conectorApiHelper.ResolverPeticionPost(environment.AddAppointment, this.currentAppointment);
      const isSuccessfull = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      if (isSuccessfull) {
        this.currentAppointment = getAppointmentModel();
        this.GetAppointmentList();
      }
    } catch (error) {
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    } finally {
      this.loading = false;
    }
  }

  public async RemoveAppointment(appointmentId) {
    this.loading = true;
    try {
      const response = this.conectorApiHelper.ResolverPeticionGet(environment.RemoveAppointment + appointmentId)
      const isSuccessfull = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      if (isSuccessfull) {
        this.currentAppointment = getAppointmentModel();
        this.GetAppointmentList();
      }
    } catch (error) {
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(error);
      this.toasterService.pop('error', 'Error', errorMessagge);
    } finally {
      this.loading = false;
    }
  }
}

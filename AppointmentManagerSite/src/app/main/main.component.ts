import { Component, OnInit, ViewChild } from '@angular/core';
import { PatientComponent } from '../patient/patient.component';
import { AppointmentComponent } from '../appointment/appointment.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  @ViewChild('patientComponent') patientComponent: PatientComponent;
  @ViewChild('appointmentComponent') appointmentComponent: AppointmentComponent;

  constructor() { }

  ngOnInit() {
  }



}

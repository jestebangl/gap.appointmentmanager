import { Component, OnInit } from '@angular/core';
import { PatientColumDefHelper } from '../Core/Constants/patientColumDefHelper';
import { MessagesHelper } from '../Core/Constants/messagesHelper';
import { ConectorApiHelper } from '../Core/Helper/conectorApiHelper';
import { ToasterService } from 'angular2-toaster';
import { environment } from 'src/environments/environment';
import { FormGroup } from '@angular/forms';
import { getPatientFormGroup, getGeneralUtilities, getPatientModel } from '../Core/Factory/ProjectFactory';
import { GeneralUtilitiesHelper } from '../Core/Helper/generalUtilitiesHelper';
import { PatientModel } from '../Core/Models/PatientModel';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  public loading = false;
  public patientList: any;
  public PATIENT_COLUMN_DEF = PatientColumDefHelper;
  public MESSAGGES_HELPER = MessagesHelper;
  public patientForm: FormGroup;
  public currentPatient: PatientModel;
  public generalUtilities: GeneralUtilitiesHelper;
  public documentTypeList: any;

  private toasterService: ToasterService;


  constructor(private conectorApiHelper: ConectorApiHelper,
    toasterService: ToasterService) {
    this.toasterService = toasterService;
    this.currentPatient = getPatientModel();
    this.patientForm = getPatientFormGroup(this.currentPatient);
    this.generalUtilities = getGeneralUtilities();

    this.GetPacientsList();
    this.GetDocumentTypeList();
  }

  ngOnInit() {
  }

  private async GetPacientsList() {
    this.loading = true;
    try {
      const respuesta = await this.conectorApiHelper.ResolverPeticionGet(environment.GetPatientList);
      this.patientList = this.conectorApiHelper.ObtenerBodyRespuesta(respuesta);
    } catch (e) {
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(e);
      this.toasterService.pop('error', 'Error', errorMessagge);
    } finally {
      this.loading = false;
    }
  }

  private async GetDocumentTypeList() {
    this.loading = true;
    try {
      const respuesta = await this.conectorApiHelper.ResolverPeticionGet(environment.GetDocumentType);
      this.documentTypeList = this.conectorApiHelper.ObtenerBodyRespuesta(respuesta);
    } catch (e) {
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(e);
      this.toasterService.pop('error', 'Error', errorMessagge);
    } finally {
      this.loading = false;
    }
  }

  public async GetPatientById(patientId) {
    this.loading = true;
    try {
      const respuesta = await this.conectorApiHelper.ResolverPeticionGet(environment.GetPatientById + patientId);
      this.currentPatient = this.conectorApiHelper.ObtenerBodyRespuesta(respuesta);
    } catch (e) {
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(e);
      this.toasterService.pop('error', 'Error', errorMessagge);
    } finally {
      this.loading = false;
    }
  }

  public async SendPatientForm() {
    this.loading = true;

    let url = '';

    if (this.currentPatient.PatientId === 0) {
      url = environment.AddPatient;
    } else {
      url = environment.UpdatePatient;
    }

    try {
      const response = await this.conectorApiHelper.ResolverPeticionPost(url, this.currentPatient);
      const isSuccessfull = this.conectorApiHelper.ObtenerBodyRespuesta(response);
      if (isSuccessfull) {
        this.currentPatient = getPatientModel();
        this.GetPacientsList();
      }
    } catch (e) {
      const errorMessagge = this.conectorApiHelper.ObtenerBodyRespuestaError(e);
      this.toasterService.pop('error', 'Error', errorMessagge);
    } finally {
      this.loading = false;
    }

  }
}

﻿namespace Gap.AppointmentManager.Domain.Entities.Helper
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;


    public class EncriptationHelper
    {
        private static readonly Encoding AppDefaultEncoding = Encoding.GetEncoding("UTF-8");
        public static string AES_SALT = "AES_SALT";
        public static string AES_IV = "AES_IV";
        //private static Encoding AppIso88591Encoding = Encoding.GetEncoding("ISO-8859-1");

        public static string EncryptData(string key, string data)
        {
            TripleDESCryptoServiceProvider provider = new TripleDESCryptoServiceProvider
            {
                Mode = CipherMode.CFB,
                Key = AppDefaultEncoding.GetBytes(key),
                IV = AppDefaultEncoding.GetBytes(key.Substring(0, 8))
            };

            ICryptoTransform encryptor = provider.CreateEncryptor();

            string res;
            using (MemoryStream ms = new MemoryStream())
            using (CryptoStream stream = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
            {
                byte[] bytes = Encoding.Unicode.GetBytes(data);
                stream.Write(bytes, 0, bytes.Length);

                stream.FlushFinalBlock();

                if (ms.Length == 0)
                {
                    res = string.Empty;
                }
                else
                {
                    byte[] buff = ms.ToArray();
                    res = Convert.ToBase64String(buff, 0, buff.Length);
                }

                stream.Close();
            }

            return res;
        }

        public static byte[] EncryptData(string key, byte[] bytes)
        {
            TripleDESCryptoServiceProvider provider = new TripleDESCryptoServiceProvider
            {
                Mode = CipherMode.CFB,
                Key = AppDefaultEncoding.GetBytes(key),
                IV = AppDefaultEncoding.GetBytes(key.Substring(0, 8))
            };

            ICryptoTransform encryptor = provider.CreateEncryptor();

            byte[] res = new byte[0];
            using (MemoryStream ms = new MemoryStream())
            using (CryptoStream stream = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
            {
                stream.Write(bytes, 0, bytes.Length);

                stream.FlushFinalBlock();

                if (ms.Length != 0)
                {
                    res = ms.ToArray();
                }

                stream.Close();
            }
            return res;
        }

        public static string DecryptData(string key, string data)
        {
            try
            {
                TripleDESCryptoServiceProvider provider = new TripleDESCryptoServiceProvider
                {
                    Mode = CipherMode.CFB,
                    Key = AppDefaultEncoding.GetBytes(key),
                    IV = AppDefaultEncoding.GetBytes(key.Substring(0, 8))
                };

                ICryptoTransform decryptor = provider.CreateDecryptor();

                string result;
                using (MemoryStream ms = new MemoryStream())
                using (CryptoStream stream = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
                {
                    char[] carray = data.ToCharArray();
                    byte[] bytes = Convert.FromBase64CharArray(carray, 0, carray.Length);
                    stream.Write(bytes, 0, bytes.Length);

                    stream.FlushFinalBlock();

                    result = new UnicodeEncoding().GetString(ms.ToArray());

                    stream.Close();
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Ocurrió un error al intentar hacer un descifrado. Data: " + data);
            }
        }

        public static byte[] DecryptData(string key, byte[] bytes)
        {
            TripleDESCryptoServiceProvider provider = new TripleDESCryptoServiceProvider
            {
                Mode = CipherMode.CFB,
                Key = AppDefaultEncoding.GetBytes(key),
                IV = AppDefaultEncoding.GetBytes(key.Substring(0, 8))
            };

            ICryptoTransform decryptor = provider.CreateDecryptor();

            byte[] result;
            using (MemoryStream ms = new MemoryStream())
            using (CryptoStream stream = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
            {
                stream.Write(bytes, 0, bytes.Length);

                stream.FlushFinalBlock();

                result = ms.ToArray();

                stream.Close();
            }

            return result;
        }

        public static string CrypthSHA384(string data)
        {
            byte[] dataAsBytes = Encoding.UTF8.GetBytes(data);
            SHA384 shaM = new SHA384Managed();
            byte[] result = shaM.ComputeHash(dataAsBytes);
            string hex = BitConverter.ToString(result);
            return hex.Replace("-", "").ToLower();
        }

        private static string getString(byte[] b)
        {
            return Convert.ToBase64String(b);//new UnicodeEncoding().GetString(b);//Encoding.UTF8.GetString(b, 0, b.Length); //Encoding.Default.GetString(b);//Encoding.UTF8.GetString(b);
        }

        /// <summary>
        /// Encrypts the aes256.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="password">The password.</param>
        /// <param name="method">The method. Enabled by AES_SALT (DecryptAes256Salt), AES_IV (DecryptAes256Iv) and string.Empty by SimpleAes256</param>
        /// <param name="iv">The iv.</param>
        /// <param name="salt">The salt.</param>
        /// <returns></returns>
        public static string EncryptAes256(string data, string password, string method = "", string iv = "", string salt = "")
        {
            string result = "";

            switch (method)
            {
                case "AES_SALT":
                    result = EncryptAes256Salt(data, password);
                    break;

                case "AES_IV":
                    result = EncryptAes256Iv(data, password, iv);
                    break;

                default:
                    result = EncryptSimpleAes256(data, password);
                    break;
            }

            return result;

        }

        /// <summary>
        /// Decrypts the aes256.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="password">The password.</param>
        /// <param name="method">The method. Enabled by AES_SALT (DecryptAes256Salt), AES_IV (DecryptAes256Iv) and string.Empty by SimpleAes256</param>
        /// <param name="iv">The iv.</param>
        /// <param name="salt">The salt.</param>
        /// <returns></returns>
        public static string DecryptAes256(string data, string password, string method = "", string iv = "", string salt = "")
        {
            string result = "";

            switch (method)
            {
                case "AES_SALT":
                    result = DecryptAes256Salt(data, password);
                    break;

                case "AES_IV":
                    result = DecryptAes256Iv(data, password, iv);
                    break;

                default:
                    result = DecryptSimpleAes256(data, password);
                    break;
            }

            return result;
        }

        #region SimpleAes256      
        /// https://codereview.stackexchange.com/questions/154017/aes256-implementation
        /// https://msdn.microsoft.com/es-es/library/system.security.cryptography.aescryptoserviceprovider(v=vs.110).aspx
        /// <summary>
        /// Encrypts the simple aes256.
        /// </summary>
        /// <param name="dataIn">The data in.</param>
        /// <param name="keyIn">The key in.</param>
        /// <returns></returns>
        public static string EncryptSimpleAes256(string dataIn, string keyIn)
        {
            byte[] data = Encoding.UTF8.GetBytes(dataIn);
            byte[] key = Encoding.UTF8.GetBytes(keyIn);
            byte[] encrypted;


            using (AesCryptoServiceProvider csp = new AesCryptoServiceProvider())
            {
                //csp.KeySize = 256;
                csp.BlockSize = 128;
                csp.Key = key;
                //csp.Padding = PaddingMode.PKCS7;
                csp.Mode = CipherMode.ECB;
                ICryptoTransform encrypter = csp.CreateEncryptor();

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encrypter, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(dataIn);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return getString(encrypted);
        }

        private static string DecryptSimpleAes256(string dataIn, string keyIn)
        {
            byte[] data = Convert.FromBase64String(dataIn);
            byte[] key = Encoding.UTF8.GetBytes(keyIn);

            using (AesCryptoServiceProvider csp = new AesCryptoServiceProvider())
            {
                //csp.KeySize = 256;
                csp.BlockSize = 128;
                csp.Key = key;
                //csp.Padding = PaddingMode.PKCS7;
                csp.Mode = CipherMode.ECB;
                ICryptoTransform decrypter = csp.CreateDecryptor();
                //return getString(decrypter.TransformFinalBlock(data, 0, data.Length));

                string plaintext = null;

                using (MemoryStream msDecrypt = new MemoryStream(data))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decrypter, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt, Encoding.GetEncoding("iso-8859-1")))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

                return plaintext;
            }
        }

        #endregion

        #region AES256 IV

        //private const string AesIV256 = @"!QAZ2WSX#EDC4RFV";
        //private const string AesKey256 = @"5TGB&YHN7UJM(IK<5TGB&YHN7UJM(IK<";
        /// http://stackoverflow.com/questions/28613831/encrypt-decrypt-querystring-values-using-aes-256
        /// <summary>
        /// Encrypts the aes256 iv.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="key">The key.</param>
        /// <param name="iv">The iv.</param>
        /// <returns></returns>
        public static string EncryptAes256Iv(string text, string key, string iv)
        {

            if (!(iv.Length == 16))
            {
                throw new Exception("La longitud del vector de inicialización debe tener 16 caracteres");
            }

            // AesCryptoServiceProvider
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 256;
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // Convert string to byte array
            byte[] src = Encoding.Unicode.GetBytes(text);

            // encryption
            using (ICryptoTransform encrypt = aes.CreateEncryptor())
            {
                byte[] dest = encrypt.TransformFinalBlock(src, 0, src.Length);

                // Convert byte array to Base64 strings
                return Convert.ToBase64String(dest);
            }
        }

        /// <summary>
        /// AES decryption
        /// </summary>
        public static string DecryptAes256Iv(string text, string key, string iv)
        {
            if (!(iv.Length == 16))
            {
                throw new Exception("La longitud del vector de inicialización debe tener 16 caracteres");
            }

            // AesCryptoServiceProvider
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 256;
            aes.IV = Encoding.UTF8.GetBytes(iv);
            aes.Key = Encoding.UTF8.GetBytes(key);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // Convert Base64 strings to byte array
            byte[] src = System.Convert.FromBase64String(text);

            // decryption
            using (ICryptoTransform decrypt = aes.CreateDecryptor())
            {
                byte[] dest = decrypt.TransformFinalBlock(src, 0, src.Length);
                return Encoding.Unicode.GetString(dest);
            }
        }

        public static string DecryptStringFromBytes(string cipherTextStr, string keyStr, string ivStr)
        {
            byte[] cipherText = Convert.FromBase64String(cipherTextStr);
            byte[] key = Encoding.UTF8.GetBytes(keyStr);
            byte[] iv = Encoding.UTF8.GetBytes(ivStr);
            // Declare the string used to hold  
            // the decrypted text.  
            string plaintext = null;

            // Create an RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings  
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.  
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream  
                                // and place them in a string.  
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }

        #endregion

        #region AES256 Methods - SALT KEY
        /// https://github.com/FernandoPucci/CryptAES256/blob/master/CryptAES256/CryptAES256/Helper/CryptUtils.cs
        /// <summary>
        /// Generate SALT - https://en.wikipedia.org/wiki/Salt_(cryptography)
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        private static string GenerateSaltKey(string password)
        {
            Rfc2898DeriveBytes rfc2898db = new Rfc2898DeriveBytes(password, 16, 10000);

            byte[] data = new byte[48];
            Buffer.BlockCopy(rfc2898db.Salt, 0, data, 0, 16);
            Buffer.BlockCopy(rfc2898db.GetBytes(32), 0, data, 16, 32);
            return Convert.ToBase64String(data);
        }

        /// <summary>
        /// Generete a key
        /// </summary>
        /// <param name="password"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        private static byte[] GenerateKey(string password, byte[] salt)
        {
            Rfc2898DeriveBytes rfc2898db = new Rfc2898DeriveBytes(password, salt, 10000);
            return rfc2898db.GetBytes(32);
        }

        private static string EncryptAes256Salt(string plain, string password)
        {
            if (plain == null || plain.Length == 0) return null;

            byte[] encrypted;
            byte[] data = Encoding.UTF8.GetBytes(plain);

            string saltKeyStr = GenerateSaltKey(password);
            byte[] saltKeyB = Convert.FromBase64String(saltKeyStr);
            byte[] salt = new byte[16];
            byte[] key = new byte[32];
            Buffer.BlockCopy(saltKeyB, 0, salt, 0, 16);
            Buffer.BlockCopy(saltKeyB, 16, key, 0, 32);
            saltKeyStr = null;
            saltKeyB = null;

            using (MemoryStream ms = new MemoryStream())
            {
                using (AesCryptoServiceProvider aes256 = new AesCryptoServiceProvider())
                {
                    aes256.KeySize = 256;
                    aes256.BlockSize = 128;
                    aes256.GenerateIV();
                    aes256.Padding = PaddingMode.PKCS7;
                    aes256.Mode = CipherMode.CBC;
                    aes256.Key = key;
                    key = null;

                    using (CryptoStream cs = new CryptoStream(ms, aes256.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        ms.Write(aes256.IV, 0, aes256.IV.Length);
                        ms.Write(salt, 0, 16);
                        cs.Write(data, 0, plain.Length);
                    }
                }

                encrypted = ms.ToArray();
            }

            return Convert.ToBase64String(encrypted);
        }

        private static string DecryptAes256Salt(string cipher, string password)
        {
            if (cipher == null || cipher.Length == 0) return null;

            byte[] decrypted;
            byte[] data = Convert.FromBase64String(cipher);

            using (MemoryStream ms = new MemoryStream(data))
            {
                using (AesCryptoServiceProvider aes256 = new AesCryptoServiceProvider())
                {
                    byte[] iv = new byte[16];
                    byte[] salt = new byte[16];
                    ms.Read(iv, 0, 16);
                    ms.Read(salt, 0, 16);

                    aes256.KeySize = 256;
                    aes256.BlockSize = 128;
                    aes256.IV = iv;
                    aes256.Padding = PaddingMode.PKCS7;
                    aes256.Mode = CipherMode.CBC;
                    aes256.Key = GenerateKey(password, salt);

                    using (var cs = new CryptoStream(ms, aes256.CreateDecryptor(), CryptoStreamMode.Read))
                    {
                        byte[] temp = new byte[ms.Length - 16 - 16 + 1];
                        decrypted = new byte[cs.Read(temp, 0, temp.Length)];
                        Buffer.BlockCopy(temp, 0, decrypted, 0, decrypted.Length);
                    }
                }
            }

            return Encoding.UTF8.GetString(decrypted);
        }

        #endregion

    }
}

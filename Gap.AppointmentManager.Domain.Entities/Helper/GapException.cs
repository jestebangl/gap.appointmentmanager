﻿using System.Resources;

[assembly: NeutralResourcesLanguage("es")]
namespace Gap.AppointmentManager.Domain.Entities.Helper
{
    using Gap.AppointmentManager.Domain.Entities.Enum;
    using System;
    using System.Runtime.Serialization;

    public class GapException : Exception
    {
        public GapException() : base() { }

        public GapException(string message) : base(message)
        {
        }

        /// <summary>
        /// Thrown a new GapException with a message for the user
        /// </summary>
        /// <param name="methodException">Current exception method</param>
        /// <param name="userExceptionMessage">Message to throw to the user</param>
        /// <param name="ex">Exception when ocurrs</param>
        public GapException(string methodException, ErrorMessagesEnum userExceptionMessage, Exception ex = null)
        {
            if (ex != null)
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {ex.ToString()}");
            }
            else
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {userExceptionMessage.ToDescriptionString()}");
            }

            throw new GapException(userExceptionMessage.ToDescriptionString());
        }

        /// <summary>
        /// Thrown a new GapException with a message for the user
        /// </summary>
        /// <param name="methodException">Current exception method</param>
        /// <param name="userExceptionMessage">Message to throw to the user</param>
        /// <param name="ex">Exception when ocurrs</param>
        public GapException(string methodException, string userExceptionMessage, Exception ex = null)
        {
            if (ex != null)
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {ex.ToString()}");
            }
            else
            {
                Logger.Logger.Current.Error($"Error at: {methodException}: Error => {userExceptionMessage}");
            }

            throw new GapException(userExceptionMessage);
        }

        protected GapException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}

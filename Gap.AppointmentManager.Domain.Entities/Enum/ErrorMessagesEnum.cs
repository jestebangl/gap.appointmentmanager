﻿namespace Gap.AppointmentManager.Domain.Entities.Enum
{
    using System.ComponentModel;

    public enum ErrorMessagesEnum
    {
        [Description("Right now there is a problem with our services, please try again later.")]
        UnexpectedError = 1,
        [Description("We are having problems getting the documents type, please try again later.")]
        GetDocumentTypeError = 2,
        [Description("We are having problems getting the appointment type, please try again later.")]
        GetAppointmentTypeError = 3,
        [Description("We are having problems adding the new patient, please try again later.")]
        AddingPatientError = 4,
        [Description("We are having problems updating the new patient, please try again later.")]
        UpdatingPatientError = 5,
        [Description("We are having problems retrieving the patients list, please try again later.")]
        GettingPatientsListError = 6,
        [Description("We are having problems retrieving the patient by id, please try again later.")]
        GettingPatientsByIdError = 7,
        [Description("There is already a patient with the same document number.")]
        PatientWithSameDocumentNumber = 8,
        [Description("Cant add a new appointment to the patient in this day because already has one assigned.")]
        CantAddNewAppointment = 9,
        [Description("Cant cancel the appointment a this moment.")]
        CantCancelAppointment = 10


    }

    public static partial class ErrorMessageEnumExtention
    {
        public static string ToDescriptionString(this ErrorMessagesEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}

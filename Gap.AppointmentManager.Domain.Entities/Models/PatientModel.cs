﻿namespace Gap.AppointmentManager.Domain.Entities
{
    public class PatientModel
    {
        public long PatientId { get; set; }

        public string PatientName { get; set; }

        public string PatientLastName { get; set; }

        public long? DocuementTypeId { get; set; }

        public string DocumentNumber { get; set; }

        public string Email { get; set; }

        public string CellPhoneNumber { get; set; }

        public PatientModel() { }
    }
}

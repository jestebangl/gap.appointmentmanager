﻿namespace Gap.AppointmentManager.Domain.Entities.Models
{
    public class AppointmentTypeModel
    {
        public long AppointmentTypeId { get; set; }

        public string AppointmentTypeName { get; set; }

        public AppointmentTypeModel() { }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gap.AppointmentManager.Domain.Entities.Models
{
    public class AppointmentModel
    {
        public long AppointmentId { get; set; }

        public string PatientLastName { get; set; }

        public string PatientName { get; set; }

        public DateTime AppointmentDate { get; set; }

        public string AppointmentTypeName { get; set; }

        public AppointmentModel()
        {

        }

    }
}

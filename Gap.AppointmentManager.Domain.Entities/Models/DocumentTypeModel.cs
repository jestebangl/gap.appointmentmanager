﻿namespace Gap.AppointmentManager.Domain.Entities.Models
{
    public class DocumentTypeModel
    {
        public long DocumentTypeId { get; set; }

        public string DocumentName { get; set; }

        public DocumentTypeModel()
        {

        }
    }
}

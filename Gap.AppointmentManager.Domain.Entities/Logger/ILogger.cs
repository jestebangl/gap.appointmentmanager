﻿namespace Gap.AppointmentManager.Domain.Entities.Logger
{
    using System;
    using System.Collections.Generic;

    public interface ILogger
    {

        /// <summary>
        /// Debugs the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Debug(string message);

        /// <summary>
        /// Errors the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        void Error(string message, Exception ex);

        /// <summary>
        /// Errors the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Error(string message);

        /// <summary>
        /// Info the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Info(string message);

        /// <summary>
        /// Warns the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        void Warn(string message, Exception ex);

        /// <summary>
        /// Warns the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Warn(string message);

        /// <summary>
        /// Fatal the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        void Fatal(string message);

        /// <summary>
        /// Fatal the specified message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        void Fatal(string message, Exception ex);

        /// <summary>
        /// Sets the thread custom string properties.
        /// </summary>
        /// <param name="properties">The properties.</param>
        void SetThreadCustomStringProperties(IDictionary<string, string> properties);

        /// <summary>
        /// Sets the thread custom date time properties.
        /// </summary>
        /// <param name="properties">The properties.</param>
        void SetThreadCustomDateTimeProperties(IDictionary<string, DateTime> properties);

        /// <summary>
        /// Sets the general custom properties.
        /// </summary>
        /// <param name="properties">The properties.</param>
        void SetGeneralCustomProperties(IDictionary<string, string> properties);

        /// <summary>
        /// Gets the current method.
        /// </summary>
        /// <returns></returns>
        string GetCurrentMethod();

    }
}

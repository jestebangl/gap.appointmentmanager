﻿namespace Gap.AppointmentManager.Presentation.WebApi.Filters
{
    using Gap.AppointmentManager.Domain.Entities.Helper;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;

    public class ResponseWrappingHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            string str = request.Content.ReadAsStringAsync().Result;
            var decriptedFromJavascript = str;
            IEnumerable<string> esCifrado = new List<string>();

            request.Headers.TryGetValues("EsCifrado", out esCifrado);

            if (esCifrado != null && bool.Parse(esCifrado.FirstOrDefault()) == true && !string.IsNullOrEmpty(str))
            {
                decriptedFromJavascript = EncriptationHelper.DecryptStringFromBytes(str, cipherKey(), cipherIv());
            }

            request.Content = new StringContent(decriptedFromJavascript);
            request.Content.Headers.ContentType.MediaType = "application/json";

            var response = await base.SendAsync(request, cancellationToken);

            return BuildApiResponse(request, response);
        }

        /// <summary>The build api response.</summary>
        /// <param name="request">The request.</param>
        /// <param name="response">The response.</param>
        /// <returns>The <see cref="HttpResponseMessage"/>.</returns>
        private HttpResponseMessage BuildApiResponse(HttpRequestMessage request, HttpResponseMessage response)
        {
            bool showError = Convert.ToBoolean(ConfigurationManager.AppSettings["ExplicitShowErrors"]);

            object content;
            List<string> modelStateErrors = new List<string>();
            var success = true;

            if (response.TryGetContentValue(out content) && !response.IsSuccessStatusCode)
            {
                HttpError error = content as HttpError;
                if (error != null)
                {
                    content = null;
                    success = false;
                    var httpErrorObject = response.Content.ReadAsStringAsync().Result;
                    if (error.ModelState != null)
                    {

                        var anonymousErrorObject = new { message = "", ModelState = new Dictionary<string, string[]>() };

                        var deserializedErrorObject = JsonConvert.DeserializeAnonymousType(
                            httpErrorObject,
                            anonymousErrorObject);

                        var modelStateValues = deserializedErrorObject.ModelState.Select(kvp => string.Join(". ", kvp.Value));

                        for (int i = 0; i < modelStateValues.Count(); i++)
                        {
                            modelStateErrors.Add(modelStateValues.ElementAt(i));
                        }
                    }
                    else if (!response.IsSuccessStatusCode)
                    {
                        //response.StatusCode = (content == null) ? System.Net.HttpStatusCode.NoContent : response.StatusCode;       
                        var anonymousErrorObject = new { Message = string.Empty, ExceptionMessage = string.Empty };
                        var deserializedErrorObject = JsonConvert.DeserializeAnonymousType(httpErrorObject, anonymousErrorObject);
                        if (showError && !string.IsNullOrWhiteSpace(deserializedErrorObject.ExceptionMessage))
                        {
                            string[] listErrors = deserializedErrorObject.ExceptionMessage.Split('|');
                            if (listErrors.Length > 0)
                            {

                                modelStateErrors = listErrors.ToList();
                            }
                            else
                            {
                                modelStateErrors.Add(deserializedErrorObject.ExceptionMessage);
                            }
                        }
                        else
                        {
                            modelStateErrors.Add(deserializedErrorObject.Message);
                        }
                    }
                }
            }

            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                modelStateErrors.Add("Credenciales inválidas.");
                success = false;
            }


            string contentSerialize = JsonConvert.SerializeObject(content);
            var contentBase64 = System.Text.Encoding.UTF8.GetBytes(contentSerialize);
            var contentBase64String = Convert.ToBase64String(contentBase64);

            IEnumerable<string> esCifrado = new List<string>();
            var newResponse = new HttpResponseMessage();

            request.Headers.TryGetValues("EsCifrado", out esCifrado);

            if (esCifrado != null && bool.Parse(esCifrado.FirstOrDefault()) == true)
            {
                newResponse = request.CreateResponse(response.StatusCode, new WebApiResponseModel(EncriptationHelper.EncryptAes256Iv(contentBase64String, cipherKey(), cipherIv()), modelStateErrors, success));
            }
            else
            {
                newResponse = request.CreateResponse(response.StatusCode, new WebApiResponseModel(content, modelStateErrors, success));
            }

            foreach (var header in response.Headers)
            {
                newResponse.Headers.Add(header.Key, header.Value);
            }

            return newResponse;
        }

        private string cipherKey()
        {
            var passCipher = ConfigurationManager.AppSettings.Get("webApiPassCipher");

            var base64EncodedPassCipherPassOne = System.Convert.FromBase64String(passCipher.ToString());
            var cipherPassOne = System.Text.Encoding.UTF8.GetString(base64EncodedPassCipherPassOne);

            var base64EncodedPassCipher = System.Convert.FromBase64String(cipherPassOne.ToString());
            return System.Text.Encoding.UTF8.GetString(base64EncodedPassCipher);
        }

        private string cipherIv()
        {
            var passIv = ConfigurationManager.AppSettings.Get("webApiPassIv");

            var base64EncodedPassIvPassOne = System.Convert.FromBase64String(passIv.ToString());
            var ivPassOne = System.Text.Encoding.UTF8.GetString(base64EncodedPassIvPassOne);

            var base64EncodedPassIv = System.Convert.FromBase64String(ivPassOne.ToString());
            return System.Text.Encoding.UTF8.GetString(base64EncodedPassIv);
        }
    }
}
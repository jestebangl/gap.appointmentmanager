﻿namespace Gap.AppointmentManager.Presentation.WebApi.Controllers
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Interfaces.Service;
    using System.Web.Http;

    [CustomCorsPolicy]
    public class PatientController : ApiController
    {
        public IPatientService PatientService { get; set; }

        public PatientController(IPatientService PatientService)
        {
            this.PatientService = PatientService;
        }

        [HttpPost]
        [Route("api/patient/addpatient")]
        public IHttpActionResult AddPatient(Patient patient)
        {
            return Ok(PatientService.AddPatient(patient));
        }

        [HttpGet]
        [Route("api/patient/getpatientbyid")]
        public IHttpActionResult GetPatientById(long patientId)
        {
            return Ok(PatientService.GetPatientById(patientId));
        }

        [HttpGet]
        [Route("api/patient/getpatientlist")]
        public IHttpActionResult GetPatientList()
        {
            return Ok(PatientService.GetPatientList());
        }

        [HttpPost]
        [Route("api/patient/updatepatient")]
        public IHttpActionResult UpdatePatient(Patient patient)
        {
            return Ok(PatientService.UpdatePatient(patient));
        }

    }
}

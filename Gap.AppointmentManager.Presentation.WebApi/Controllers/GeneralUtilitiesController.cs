﻿namespace Gap.AppointmentManager.Presentation.WebApi.Controllers
{
    using Gap.AppointmentManager.Domain.Interfaces.Service;
    using System.Web.Http;

    [CustomCorsPolicy]
    public class GeneralUtilitiesController : ApiController
    {
        public IGeneralUtilitiesService GeneralUtilitiesService { get; set; }

        public GeneralUtilitiesController(IGeneralUtilitiesService GeneralUtilitiesService)
        {
            this.GeneralUtilitiesService = GeneralUtilitiesService;
        }

        [HttpGet]
        [Route("api/generalutilitites/getdocumenttype")]
        public IHttpActionResult GetDocumentType()
        {
            return Ok(GeneralUtilitiesService.GetDocumentTypes());
        }

        [HttpGet]
        [Route("api/generalutilitites/getappointmenttype")]
        public IHttpActionResult GetAppointmentType()
        {
            return Ok(GeneralUtilitiesService.GetAppointmentType());
        }
    }
}

﻿namespace Gap.AppointmentManager.Presentation.WebApi.Controllers
{
    using System.Web.Mvc;

    [CustomCorsPolicy]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}

﻿namespace Gap.AppointmentManager.Presentation.WebApi.Controllers
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Interfaces.Service;
    using System.Web.Http;

    public class AppointmentController : ApiController
    {
        public IAppointmentService AppointmentService { get; set; }

        public AppointmentController(IAppointmentService AppointmentService)
        {
            this.AppointmentService = AppointmentService;
        }

        [HttpGet]
        [Route("api/appointment/getappointmentlist")]
        public IHttpActionResult GetAppointmentList()
        {
            return Ok(AppointmentService.GetAppointmentList());
        }

        [HttpPost]
        [Route("api/appointment/addappointment")]
        public IHttpActionResult AddAppointment(Appointment appointment)
        {
            return Ok(AppointmentService.AddAppointment(appointment));
        }

        [HttpGet]
        [Route("api/appointment/removeappointment/")]
        public IHttpActionResult RemoveAppointment(long appointmentId)
        {
            return Ok(AppointmentService.RemoveAppointment(appointmentId));
        }

    }
}

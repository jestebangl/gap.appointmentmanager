﻿namespace Gap.AppointmentManager.Presentation.WebApi
{
    using System;
    using System.Configuration;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Cors;
    using System.Web.Http.Cors;

    /// <summary>
    /// Custom implementation of ICorsPolicyProvider that loads the origins from web.config
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false)]
    public class CustomCorsPolicyAttribute : Attribute, ICorsPolicyProvider
    {
        private CorsPolicy _policy;

        /// <summary>
        /// Gets allowed origins from web.config
        /// </summary>
        public CustomCorsPolicyAttribute()
        {
            _policy = new CorsPolicy
            {
                AllowAnyMethod = true,
                AllowAnyHeader = true
            };

            // loads the origins from AppSettings

            var originsAllowed = ConfigurationManager.AppSettings["cors:allowOrigins"];

            if (!string.IsNullOrEmpty(originsAllowed))
            {
                foreach (var origin in originsAllowed.Split(','))
                {
                    _policy.Origins.Add(origin.Replace("\r", string.Empty).Replace("\n", string.Empty).Trim());
                }
            }
        }

        public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancel)
        {
            return Task.FromResult(_policy);
        }
    }
}
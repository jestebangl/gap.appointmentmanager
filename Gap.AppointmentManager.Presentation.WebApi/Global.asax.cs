﻿namespace Gap.AppointmentManager.Presentation.WebApi
{
    using Castle.MicroKernel.Lifestyle;
    using Castle.MicroKernel.Resolvers.SpecializedResolvers;
    using Gap.AppointmentManager.Application.Validator.Validators;
    using Gap.AppointmentManager.Infrastructure.DataAccess.Repository;
    using Gap.AppointmentManager.Presentation.WebApi.Windsor;
    using Gap.AppointmentManager.Service.DependencyResolution.Installers;
    using Newtonsoft.Json;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            InitializeIoC(GlobalConfiguration.Configuration);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Re‌​ferenceLoopHandling = ReferenceLoopHandling.Ignore;
        }

        public static void InitializeIoC(HttpConfiguration configuration)
        {
            var UnitOfWork = new UnitOfWork();
            var PatientValidator = new PatientValidator(UnitOfWork);
            var AppointmentValidator = new AppointmentValidator(UnitOfWork);

            var windsor = Ragolo.Core.IoC.IocHelper.Instance;
            var contenedor = windsor.GetContainer();

            windsor.Install(new ApplicationValidatorInstaller(PatientValidator, AppointmentValidator));
            windsor.Install(new ApplicationInstaller(UnitOfWork));
            windsor.Install(new ServiceInstaller());

            windsor.Install(new Windsor.Installer.WebApiInstaller());

            WindsorControllerFactory controllerFactory = new WindsorControllerFactory(contenedor.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);

            contenedor.Kernel.Resolver.AddSubResolver(new CollectionResolver(contenedor.Kernel, true));
            contenedor.BeginScope();
            var dependencyResolver = new WindsorDependencyResolver(contenedor);
            configuration.DependencyResolver = dependencyResolver;
        }
    }
}

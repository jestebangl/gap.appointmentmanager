﻿namespace Gap.AppointmentManager.Presentation.WebApi.Windsor.Installer
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;
    using System.Web.Http.Controllers;
    using System.Web.Mvc;

    public class WebApiInstaller : IWindsorInstaller
    {
        public WebApiInstaller()
        {

        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {

            container.Register(
                Classes.
                    FromThisAssembly().
                    BasedOn<IController>(). //MVC
                    If(c => c.Name.EndsWith("Controller")).
                    LifestylePerWebRequest());

            container.Register(
                Classes.
                    FromThisAssembly().
                    BasedOn<IHttpController>(). //Web API
                    If(c => c.Name.EndsWith("Controller")).
                    LifestylePerWebRequest());
        }
    }
}
﻿namespace Gap.Appointment.Application.Operator.Patient
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities;
    using Gap.AppointmentManager.Domain.Interfaces.Application;
    using Gap.AppointmentManager.Infrastructure.DataAccess.Repository;
    using System.Collections.Generic;
    using System.Linq;

    public class PatientApplication : IPatientApplication
    {
        public IPatientHelper PatientHelper { get; set; }

        public UnitOfWork UnitOfWork { get; set; }

        public PatientApplication(UnitOfWork UnitOfWork, IPatientHelper PatientHelper)
        {
            this.UnitOfWork = UnitOfWork;
            this.PatientHelper = PatientHelper;
        }

        public bool AddPatient(Patient patient)
        {
            UnitOfWork.PatientRepository.Insert(patient);
            UnitOfWork.Save();
            return true;
        }

        public bool UpdatePatient(Patient patient)
        {
            var patientModel = UnitOfWork.PatientRepository.GetByID(patient.PatientId);
            PatientHelper.GetPatientModelToUpdate(patientModel, patient);

            UnitOfWork.PatientRepository.Update(patientModel);
            UnitOfWork.Save();
            return true;
        }

        public List<PatientModel> GetPatientList()
        {
            var patientList = UnitOfWork.PatientRepository.Get();
            return PatientHelper.GetPatientModelList(patientList);
        }

        public PatientModel GetPatientById(long patientId)
        {
            var response = UnitOfWork.PatientRepository.GetByID(patientId);
            return PatientHelper.GetPatientModel(response);
        }
    }
}

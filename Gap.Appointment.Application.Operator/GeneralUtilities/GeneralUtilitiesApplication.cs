﻿namespace Gap.Appointment.Application.Operator.GeneralUtilities
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using Gap.AppointmentManager.Domain.Interfaces.Application;
    using Gap.AppointmentManager.Infrastructure.DataAccess.Repository;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class GeneralUtilitiesApplication : IGeneralUtilitiesApplication
    {
        public UnitOfWork UnitOfWork { get; set; }

        public GeneralUtilitiesApplication(UnitOfWork UnitOfWork)
        {
            this.UnitOfWork = UnitOfWork;
        }

        public List<DocumentTypeModel> GetDocumentType()
        {
            var response = UnitOfWork.DocumentTypeRepository.Get().ToList();
            var documentTypeModel = from row in response
                                    select new DocumentTypeModel()
                                    {
                                        DocumentName = row.DocumentName,
                                        DocumentTypeId = row.DocumentTypeId
                                    };
            return documentTypeModel.ToList();
        }

        public List<AppointmentTypeModel> GetAppointmentType()
        {
            var response = UnitOfWork.AppointmentTypeRepository.Get().ToList();
            var appointmentTypeModel = from row in response
                                       select new AppointmentTypeModel()
                                       {
                                           AppointmentTypeId = row.AppointmentTypeId,
                                           AppointmentTypeName = row.AppointmentTypeName
                                       };

            return appointmentTypeModel.ToList();
        }
    }
}

﻿namespace Gap.Appointment.Application.Operator.Helper
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities;
    using Gap.AppointmentManager.Domain.Interfaces.Application;
    using System.Collections.Generic;
    using System.Linq;

    public class PatientHelper : IPatientHelper
    {
        public PatientHelper() { }

        public PatientModel GetPatientModel(Patient patient)
        {
            return new PatientModel()
            {
                CellPhoneNumber = patient.CellPhoneNumber,
                DocuementTypeId = patient.DocuementTypeId,
                DocumentNumber = patient.DocumentNumber,
                Email = patient.Email,
                PatientId = patient.PatientId,
                PatientLastName = patient.PatientLastName,
                PatientName = patient.PatientName
            };
        }


        public List<PatientModel> GetPatientModelList(IEnumerable<Patient> patientList)
        {
            var response = from row in patientList.ToList()
                           select new PatientModel()
                           {
                               CellPhoneNumber = row.CellPhoneNumber,
                               DocuementTypeId = row.DocuementTypeId,
                               DocumentNumber = row.DocumentNumber,
                               Email = row.Email,
                               PatientId = row.PatientId,
                               PatientLastName = row.PatientLastName,
                               PatientName = row.PatientName
                           };

            return response.ToList();
        }

        public Patient GetPatientModelToUpdate(Patient newReference, Patient oldReference)
        {
            newReference.CellPhoneNumber = oldReference.CellPhoneNumber;
            newReference.DocuementTypeId = oldReference.DocuementTypeId;
            newReference.DocumentNumber = oldReference.DocumentNumber;
            newReference.PatientLastName = oldReference.PatientLastName;
            newReference.PatientName = oldReference.PatientName;

            return newReference;
        }

    }
}

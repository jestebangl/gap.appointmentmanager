﻿namespace Gap.Appointment.Application.Operator.Helper
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using Gap.AppointmentManager.Domain.Interfaces.Application;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AppointmentHelper : IAppointmentHelper
    {
        public AppointmentHelper() { }

        public List<AppointmentModel> GetAppointmentModelList(IEnumerable<Appointment> appointments)
        {
            var response = from row in appointments.ToList()
                           select new AppointmentModel()
                           {
                               AppointmentDate = (DateTime)row.AppointmentDate,
                               AppointmentId = row.AppointmentId,
                               AppointmentTypeName = row.AppointmentType.AppointmentTypeName,
                               PatientLastName = row.Patient.PatientLastName,
                               PatientName = row.Patient.PatientName
                           };

            return response.ToList();
        }
    }
}

﻿namespace Gap.Appointment.Application.Operator.Appointment
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using Gap.AppointmentManager.Domain.Entities.Models;
    using Gap.AppointmentManager.Domain.Interfaces.Application;
    using Gap.AppointmentManager.Infrastructure.DataAccess.Repository;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    public class AppointmentApplication : IAppointmentApplication
    {
        public UnitOfWork UnitOfWork { get; set; }

        public IAppointmentHelper AppointmentHelper { get; set; }

        public AppointmentApplication(UnitOfWork UnitOfWork, IAppointmentHelper AppointmentHelper)
        {
            this.UnitOfWork = UnitOfWork;
            this.AppointmentHelper = AppointmentHelper;
        }

        public bool AddAppointment(Appointment appointment)
        {
            UnitOfWork.AppointmentRepository.Insert(appointment);
            UnitOfWork.Save();
            return true;
        }


        public List<AppointmentModel> GetAppointmentList()
        {
            Func<IQueryable<Appointment>, IOrderedQueryable<Appointment>> order = source => source.OrderByDescending(x => x.AppointmentDate);
            var appointments = UnitOfWork.AppointmentRepository.Get(null, order);
            return AppointmentHelper.GetAppointmentModelList(appointments);
        }

        public Appointment GetAppointmentById(long appointmenId)
        {
            return UnitOfWork.AppointmentRepository.GetByID(appointmenId);
        }

        public bool DeleteAppointment(long appointmenId)
        {
           UnitOfWork.AppointmentRepository.Delete(appointmenId);
            return true;
        }

    }
}

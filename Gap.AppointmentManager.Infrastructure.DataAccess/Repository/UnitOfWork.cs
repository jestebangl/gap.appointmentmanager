﻿namespace Gap.AppointmentManager.Infrastructure.DataAccess.Repository
{
    using Gap.AppointmentManager.Domain.AppointmentEF;
    using System;


    public class UnitOfWork : IDisposable
    {
        private AppointmentDBModel context = new AppointmentDBModel();

        private GenericRepository<DocumentType> documentTypeRepository;
        public GenericRepository<DocumentType> DocumentTypeRepository
        {
            get
            {
                return new GenericRepository<DocumentType>(context);
            }
        }

        private GenericRepository<AppointmentType> appointmentTypeRepository;
        public GenericRepository<AppointmentType> AppointmentTypeRepository
        {
            get
            {
                return new GenericRepository<AppointmentType>(context);
            }
        }

        private GenericRepository<Patient> patientRepository;
        public GenericRepository<Patient> PatientRepository
        {
            get
            {
                return new GenericRepository<Patient>(context);
            }
        }

        private GenericRepository<Appointment> appointmentRepository;
        public GenericRepository<Appointment> AppointmentRepository
        {
            get
            {
                return new GenericRepository<Appointment>(context);
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
USE [master]
GO
/****** Object:  Database [ApointmentManager]    Script Date: 7/04/2019 10:12:01 p. m. ******/
CREATE DATABASE [ApointmentManager]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ApointmentManager', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\ApointmentManager.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ApointmentManager_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\ApointmentManager_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [ApointmentManager] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ApointmentManager].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ApointmentManager] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ApointmentManager] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ApointmentManager] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ApointmentManager] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ApointmentManager] SET ARITHABORT OFF 
GO
ALTER DATABASE [ApointmentManager] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ApointmentManager] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ApointmentManager] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ApointmentManager] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ApointmentManager] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ApointmentManager] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ApointmentManager] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ApointmentManager] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ApointmentManager] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ApointmentManager] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ApointmentManager] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ApointmentManager] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ApointmentManager] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ApointmentManager] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ApointmentManager] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ApointmentManager] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ApointmentManager] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ApointmentManager] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ApointmentManager] SET  MULTI_USER 
GO
ALTER DATABASE [ApointmentManager] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ApointmentManager] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ApointmentManager] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ApointmentManager] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ApointmentManager] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ApointmentManager] SET QUERY_STORE = OFF
GO
USE [ApointmentManager]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [ApointmentManager]
GO
/****** Object:  Table [dbo].[Appointment]    Script Date: 7/04/2019 10:12:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Appointment](
	[AppointmentId] [bigint] IDENTITY(1,1) NOT NULL,
	[AppointmentTypeId] [bigint] NULL,
	[PatientId] [bigint] NULL,
	[AppointmentDate] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[AppointmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppointmentType]    Script Date: 7/04/2019 10:12:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppointmentType](
	[AppointmentTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[AppointmentTypeName] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[AppointmentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DocumentType]    Script Date: 7/04/2019 10:12:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentType](
	[DocumentTypeId] [bigint] IDENTITY(1,1) NOT NULL,
	[DocumentName] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DocumentTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Patient]    Script Date: 7/04/2019 10:12:02 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patient](
	[PatientId] [bigint] IDENTITY(1,1) NOT NULL,
	[PatientName] [varchar](100) NOT NULL,
	[PatientLastName] [varchar](100) NOT NULL,
	[DocuementTypeId] [bigint] NULL,
	[DocumentNumber] [varchar](100) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[CellPhoneNumber] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AppointmentType] ON 

INSERT [dbo].[AppointmentType] ([AppointmentTypeId], [AppointmentTypeName]) VALUES (1, N'Medicina General')
INSERT [dbo].[AppointmentType] ([AppointmentTypeId], [AppointmentTypeName]) VALUES (2, N'Odontología')
INSERT [dbo].[AppointmentType] ([AppointmentTypeId], [AppointmentTypeName]) VALUES (3, N'Pediatría')
INSERT [dbo].[AppointmentType] ([AppointmentTypeId], [AppointmentTypeName]) VALUES (4, N'Neurología')
SET IDENTITY_INSERT [dbo].[AppointmentType] OFF
SET IDENTITY_INSERT [dbo].[DocumentType] ON 

INSERT [dbo].[DocumentType] ([DocumentTypeId], [DocumentName]) VALUES (1, N'CC')
INSERT [dbo].[DocumentType] ([DocumentTypeId], [DocumentName]) VALUES (2, N'NIT')
INSERT [dbo].[DocumentType] ([DocumentTypeId], [DocumentName]) VALUES (3, N'CE')
INSERT [dbo].[DocumentType] ([DocumentTypeId], [DocumentName]) VALUES (4, N'TI')
SET IDENTITY_INSERT [dbo].[DocumentType] OFF
SET IDENTITY_INSERT [dbo].[Patient] ON 

INSERT [dbo].[Patient] ([PatientId], [PatientName], [PatientLastName], [DocuementTypeId], [DocumentNumber], [Email], [CellPhoneNumber]) VALUES (4, N'Juan Esteban', N'Gonzalez', 1, N'1053817759', N'luisaf.10fer@gmail.com', N'3194470855')
SET IDENTITY_INSERT [dbo].[Patient] OFF
ALTER TABLE [dbo].[Appointment]  WITH CHECK ADD FOREIGN KEY([AppointmentTypeId])
REFERENCES [dbo].[AppointmentType] ([AppointmentTypeId])
GO
ALTER TABLE [dbo].[Appointment]  WITH CHECK ADD FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patient] ([PatientId])
GO
ALTER TABLE [dbo].[Patient]  WITH CHECK ADD FOREIGN KEY([DocuementTypeId])
REFERENCES [dbo].[DocumentType] ([DocumentTypeId])
GO
USE [master]
GO
ALTER DATABASE [ApointmentManager] SET  READ_WRITE 
GO
